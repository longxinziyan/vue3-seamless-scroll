import { createApp } from 'vue';
import App from './App.vue';
import { jsSeamlessScroll, cssSeamlessScroll } from "vue3-seamless-scroll";
const app = createApp(App);
app.use(cssSeamlessScroll);
app.use(jsSeamlessScroll);

app.mount('#app');
