# 原生css3动画滚动配置参数

- `count`

  > 动画循环次数，默认无限循环

  ```json
    type: [Number, String],
    default: "infinite",
    required: false
  ```

- `duration`

  > 动画持续时间，默认根据容器大小计算

  ```json
    type: [Number, String],
    required: false
  ```

- `cubicBezier`

  > 贝塞尔曲线

  ```json
    type: Object,
    default: {
      x1: 0,
      y1: 0,
      x2: 1,
      y2: 1,
    },
    required: false
  ```

# 示例

## 默认配置

```html
<template>
  <css-seamless-scroll :datas="datas" class="scroll">
    <div class="item" v-for="(item, index) in datas" :key="index">
      <span>{{ item.title }}</span>
      <span>{{ item.date }}</span>
    </div>
  </css-seamless-scroll>
</template>
<script>
import { defineComponent, reactive, toRefs } from "vue";

export default defineComponent({
  name: "App",
  setup() {
    const state = reactive({
      datas: [
        {
          title: "Vue3.0 无缝滚动组件展示数据第1条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第2条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第3条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第4条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第5条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第6条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第7条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第8条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第9条",
          date: Date.now(),
        },
      ]
    });
    return { ...toRefs(state) };
  },
});
</script>

<style>
.scroll {
  height: 270px;
  width: 500px;
  margin: 100px auto;
  overflow: hidden;
}

.scroll .item {
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 3px 0;
}
</style>
```

![image](videos/css版默认配置%2000_00_00-00_00_30.gif))

## 向下滚动

```html
<template>
  <css-seamless-scroll :datas="datas" direction="down" class="scroll">
    <div class="item" v-for="(item, index) in datas" :key="index">
      <span>{{ item.title }}</span>
      <span>{{ item.date }}</span>
    </div>
  </css-seamless-scroll>
</template>
<script>
import { defineComponent, reactive, toRefs } from "vue";

export default defineComponent({
  name: "App",
  setup() {
    const state = reactive({
      datas: [
        {
          title: "Vue3.0 无缝滚动组件展示数据第1条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第2条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第3条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第4条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第5条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第6条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第7条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第8条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第9条",
          date: Date.now(),
        },
      ]
    });
    return { ...toRefs(state) };
  },
});
</script>

<style>
.scroll {
  height: 270px;
  width: 500px;
  margin: 100px auto;
  overflow: hidden;
}

.scroll .item {
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 3px 0;
}
</style>
```

![image](videos/css版向下滚动%2000_00_00-00_00_30.gif)

## 向左滚动

```html
<template>
  <css-seamless-scroll :datas="datas" direction="left" class="scroll">
    <div class="item" v-for="(item, index) in datas" :key="index">
      <span>{{ item.title }}</span>
      <span>{{ item.date }}</span>
    </div>
  </css-seamless-scroll>
</template>
<script>
import { defineComponent, reactive, toRefs } from "vue";

export default defineComponent({
  name: "App",
  setup() {
    const state = reactive({
      datas: [
        {
          title: "Vue3.0 无缝滚动组件展示数据第1条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第2条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第3条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第4条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第5条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第6条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第7条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第8条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第9条",
          date: Date.now(),
        },
      ]
    });
    return { ...toRefs(state) };
  },
});
</script>

<style>
.scroll {
  height: 270px;
  width: 500px;
  margin: 100px auto;
  overflow: hidden;
}

.scroll .item {
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 3px 0;
}
</style>
```

![image](videos/css版向左滚动%2000_00_00-00_00_30.gif)

## 滚动速度

```html
<template>
  <css-seamless-scroll :datas="datas" duration="2" class="scroll">
    <div class="item" v-for="(item, index) in datas" :key="index">
      <span>{{ item.title }}</span>
      <span>{{ item.date }}</span>
    </div>
  </css-seamless-scroll>
</template>
<script>
import { defineComponent, reactive, toRefs } from "vue";

export default defineComponent({
  name: "App",
  setup() {
    const state = reactive({
      datas: [
        {
          title: "Vue3.0 无缝滚动组件展示数据第1条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第2条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第3条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第4条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第5条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第6条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第7条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第8条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第9条",
          date: Date.now(),
        },
      ]
    });
    return { ...toRefs(state) };
  },
});
</script>

<style>
.scroll {
  height: 270px;
  width: 500px;
  margin: 100px auto;
  overflow: hidden;
}

.scroll .item {
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 3px 0;
}
</style>
```

![image](videos/css版滚动速度%2000_00_00-00_00_30.gif)

## 鼠标悬停

```html
<template>
  <css-seamless-scroll :datas="datas" hover class="scroll">
   <div class="item" v-for="(item, index) in datas" :key="index">
      <span>{{ item.title }}</span>
      <span>{{ item.date }}</span>
    </div>
  </css-seamless-scroll>
</template>
<script>
import { defineComponent, reactive, toRefs } from "vue";

export default defineComponent({
  name: "App",
  setup() {
    const state = reactive({
      datas: [
        {
          title: "Vue3.0 无缝滚动组件展示数据第1条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第2条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第3条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第4条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第5条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第6条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第7条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第8条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第9条",
          date: Date.now(),
        },
      ]
    });
    return { ...toRefs(state) };
  },
});
</script>

<style>
.scroll {
  height: 270px;
  width: 500px;
  margin: 100px auto;
  overflow: hidden;
}

.scroll .item {
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 3px 0;
}
</style>
```

![image](videos/css版鼠标悬停%2000_00_00-00_00_30.gif)

## 单步停顿

```html
<template>
  <css-seamless-scroll :datas="datas" duration="30" step="9" class="scroll">
    <div class="item" v-for="(item, index) in datas" :key="index">
      <span>{{ item.title }}</span>
      <span>{{ item.date }}</span>
    </div>
  </css-seamless-scroll>
</template>
<script>
import { defineComponent, reactive, toRefs } from "vue";

export default defineComponent({
  name: "App",
  setup() {
    const state = reactive({
      datas: [
        {
          title: "Vue3.0 无缝滚动组件展示数据第1条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第2条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第3条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第4条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第5条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第6条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第7条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第8条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第9条",
          date: Date.now(),
        },
      ]
    });
    return { ...toRefs(state) };
  },
});
</script>

<style>
.scroll {
  height: 270px;
  width: 500px;
  margin: 100px auto;
  overflow: hidden;
}

.scroll .item {
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 3px 0;
}
</style>
```

![image](videos/css版单步停顿.gif)

## 单行停顿时间

```html
<template>
  <css-seamless-scroll :datas="datas" duration="60" step="9" class="scroll">
    <div class="item" v-for="(item, index) in datas" :key="index">
      <span>{{ item.title }}</span>
      <span>{{ item.date }}</span>
    </div>
  </css-seamless-scroll>
</template>
<script>
import { defineComponent, reactive, toRefs } from "vue";

export default defineComponent({
  name: "App",
  setup() {
    const state = reactive({
      datas: [
        {
          title: "Vue3.0 无缝滚动组件展示数据第1条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第2条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第3条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第4条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第5条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第6条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第7条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第8条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第9条",
          date: Date.now(),
        },
      ]
    });
    return { ...toRefs(state) };
  },
});
</script>

<style>
.scroll {
  height: 270px;
  width: 500px;
  margin: 100px auto;
  overflow: hidden;
}

.scroll .item {
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 3px 0;
}
</style>
```

![image](videos/css版单行停顿时间.gif)

## 数组属性更新

```html
<template>
  <css-seamless-scroll :datas="datas" class="scroll">
    <div class="item" v-for="(item, index) in datas" :key="index">
      <span>{{ item.title }}</span>
      <span>{{ item.date }}</span>
    </div>
  </css-seamless-scroll>
</template>
<script>
import { defineComponent, reactive, onMounted, toRefs } from "vue";

export default defineComponent({
  name: "App",
  setup() {
    const state = reactive({
      datas: [
        {
          title: "Vue3.0 无缝滚动组件展示数据第1条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第2条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第3条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第4条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第5条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第6条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第7条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第8条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第9条",
          date: Date.now(),
        },
      ]
    });
    onMounted(() => {
      setInterval(() => {
        state.datas[1].title = "我是第2条更新数据";
        state.datas[5].title = "我是第6条更新数据";
        state.datas[7].title = "我是第8条更新数据";
      }, 1000);
    });
    return { ...toRefs(state) };
  },
});
</script>

<style>
.scroll {
  height: 270px;
  width: 500px;
  margin: 100px auto;
  overflow: hidden;
}

.scroll .item {
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 3px 0;
}
</style>
```

![image](videos/css版数组属性更新%2000_00_00-00_00_30.gif)

## 数组添加数据

```html
<template>
  <css-seamless-scroll :datas="datas" class="scroll">
    <div class="item" v-for="(item, index) in datas" :key="index">
      <span>{{ item.title }}</span>
      <span>{{ item.date }}</span>
    </div>
  </css-seamless-scroll>
</template>
<script>
import { defineComponent, onMounted, reactive, toRefs } from "vue";

export default defineComponent({
  name: "App",
  setup() {
    const state = reactive({
      datas: [
        {
          title: "Vue3.0 无缝滚动组件展示数据第1条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第2条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第3条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第4条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第5条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第6条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第7条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第8条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第9条",
          date: Date.now(),
        },
      ]
    });
    onMounted(() => {
      setInterval(() => {
        state.datas.push({
          title: "我是新增的一条数据",
          date: Date.now(),
        });
      }, 2000);
    return { ...toRefs(state) };
  },
});
</script>

<style>
.scroll {
  height: 270px;
  width: 500px;
  margin: 100px auto;
  overflow: hidden;
}

.scroll .item {
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 3px 0;
}
</style>
```

![image](videos/css版数组添加数据%2000_00_00-00_00_30.gif)

## echart图表

```html
<template>
  <css-seamless-scroll :datas="datas" class="scroll">
    <div
      v-for="(item, index) in [1, 2, 3]"
      :key="index"
      class="chart"
      style="width: 360px; height: 200px"
    ></div>
    <template #html>
      <div
        v-for="(item, index) in [1, 2, 3]"
        :key="index"
        class="chart"
        style="width: 360px; height: 200px"
      ></div>
    </template>
  </css-seamless-scroll>
</template>
<script>
import { defineComponent, onMounted, reactive, toRefs } from "vue";
import * as echarts from "echarts";

export default defineComponent({
  name: "App",
  setup() {
    const state = reactive({
      chartOptions: {
        xAxis: {
          type: "category",
          data: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
        },
        yAxis: {
          type: "value",
        },
        series: [
          {
            data: [150, 230, 224, 218, 135, 147, 260],
            type: "line",
          },
        ],
      },
    });
    onMounted(() => {
      const charts = document.querySelectorAll(".chart");
      for (let index = 0; index < charts.length; index++) {
        const element = charts[index];
        echarts.init(element).setOption(state.chartOptions);
      }
    });
    return { ...toRefs(state) };
  },
});
</script>

<style>
.scroll {
  height: 270px;
  width: 500px;
  margin: 100px auto;
  overflow: hidden;
}

.scroll .item {
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 3px 0;
}
</style>
```

![image](videos/css版echart图表%2000_00_00-00_00_30.gif)
