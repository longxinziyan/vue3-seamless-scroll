import cssSeamlessScroll from './cssSeamlessScroll.vue';

cssSeamlessScroll.install = (app) => {
  app.component(cssSeamlessScroll.name, cssSeamlessScroll)
};

export default cssSeamlessScroll;
