import jsSeamlessScroll from './jsSeamlessScroll.vue';

jsSeamlessScroll.install = (app) => {
  app.component(jsSeamlessScroll.name, jsSeamlessScroll)
};

export default jsSeamlessScroll;
