import jsSeamlessScroll from "./components/jsSeamlessScroll";
import cssSeamlessScroll from "./components/cssSeamlessScroll";

const components = [
  jsSeamlessScroll,
  cssSeamlessScroll
];

const install = function (app, options = {}) {
  components.forEach(component => {
    app.component(options.name || component.name, component);
  });
}


if (typeof window !== "undefined" && window.Vue) {
  install(window.Vue);
}

export default install;

export {
  jsSeamlessScroll,
  cssSeamlessScroll
}
