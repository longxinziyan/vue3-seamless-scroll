module.exports = {
  lintOnSave: false,
  runtimeCompiler: true,
  publicPath: './',
  outputDir: 'lib',
  css: {
    extract: false,
    sourceMap: false
  }
}
