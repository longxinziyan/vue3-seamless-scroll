# vue3-seamless-scroll

Vue3.0 无缝滚动组件

> 目前组件支持上下左右无缝滚动，单步滚动，并且支持复杂图标的无缝滚动，组件支持原生 `css3动画` 滚动，目前组件支持平台与`Vue3.0`支持平台一致。

## `1.0.7`版本已不再需要单独引入样式文件

## 效果展示

![image](docs/videos/CSS3版%2000_00_00-00_00_30.gif)

## 安装

- `npm`

  ```shell
  npm install vue3-seamless-scroll --save
  ```

- `Yarn`

  ```shell
  yarn add vue3-seamless-scroll
  ```

- `browser`

  ```html
  <script src="https://unpkg.com/browse/vue3-seamless-scroll@1.0.2/lib/vue3SeamlessScroll.umd.min.js"></script>
  ```

## 组件配置

- `datas`

  > 无缝滚动列表数据，组件内部使用列表长度。

  ```json
    type: Array
    required: true
  ```

- `v-model`

  > 通过v-model控制动画滚动与停止，默认开始滚动

  ```json
    type: Boolean,
    default: true,
    required: false
  ```

- `direction`

  > 控制滚动方向，可选值`up`，`down`，`left`，`right`

  ```json
    type: String,
    default: "up",
    required: false
  ```

- `isWatch`

  > 开启数据更新监听

  ```json
    type: Boolean,
    default: true,
    required: false
  ```

- `hover`

  > 是否开启鼠标悬停

  ```json
    type: Boolean,
    default: false,
    required: false
  ```

- `limitScrollNum`

  > 开启滚动的数据量，只有列表长度大于等于该值才会滚动

  ```json
    type: [Number, String],
    default: 5,
    required: false
  ```

- `step`

  > 步进速度

  ```json
    type: [Number, String],
    required: false
  ```

### [js版 特有参数配置文档](docs/jsSeamlessScroll.md)

### [原生css3动画 特有参数组件配置文档](docs/cssSeamlessScroll.md)

## 注意项

> 当使用原生css3动画滚动组件时，单步滚动控制需要`duration`与`step`两个参数协调使用

> 需要滚动的列表所在容器必须设置样式 `overflow: hidden`;

## 使用

### 注册组件

- 全局单个组件注册 `install`

```JavaScript
  // **main.js**
  import { createApp } from 'vue';
  import App from './App.vue';
  // 单个组件引用注册
  import { jsSeamlessScroll, cssSeamlessScroll } from "vue3-seamless-scroll";
  const app = createApp(App);
  app.use(cssSeamlessScroll);
  app.use(jsSeamlessScroll);
  app.mount('#app');
```

- 全局组件注册 `install`

```JavaScript
  // **main.js**
  import { createApp } from 'vue';
  import App from './App.vue';
  // 单个组件引用注册
  import vue3SeamlessScroll from "vue3-seamless-scroll";
  const app = createApp(App);
  app.use(vue3SeamlessScroll);
  app.mount('#app');
```

- 单个.vue文件局部注册

```html
<script>
  import { defineComponent } from "vue";
  import { jsSeamlessScroll, cssSeamlessScroll } from "vue3-seamless-scroll";
   export default defineComponent({
      components: {
        jsSeamlessScroll,
        cssSeamlessScroll
      }
   })
</script>
```

### 使用组件

```html
<template>
  <js-seamless-scroll :datas="datas" v-model="scroll" class="scroll">
    <div class="item" v-for="(item, index) in datas" :key="index">
      <span>{{item.title}}</span>
      <span>{{item.date}}</span>
    </div>
  </js-seamless-scroll>

  <css-seamless-scroll :datas="datas" v-model="scroll" class="scroll">
    <div class="item" v-for="(item, index) in datas" :key="index">
      <span>{{item.title}}</span>
      <span>{{item.date}}</span>
    </div>
  </css-seamless-scroll>
</template>
<script>
import { defineComponent, reactive, toRefs } from "vue";
import { jsSeamlessScroll, cssSeamlessScroll } from "vue3-seamless-scroll";

export default defineComponent({
  name: "App",
  components: {
    jsSeamlessScroll,
    cssSeamlessScroll
  },
  setup() {
    const state = reactive({
      scroll: true,
      datas: [
        {
          title: "Vue3.0 无缝滚动组件展示数据第1条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第2条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第3条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第4条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第5条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第6条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第7条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第8条",
          date: Date.now(),
        },
        {
          title: "Vue3.0 无缝滚动组件展示数据第9条",
          date: Date.now(),
        },
      ]
    });
    return { ...toRefs(state) };
  },
});
</script>

<style>
.scroll {
  height: 270px;
  width: 500px;
  margin: 100px auto;
  overflow: hidden;
}

.scroll .item {
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 3px 0;
}
</style>

```
