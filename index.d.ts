declare module 'vue3-seamless-scroll' {
  type cssOptions<D = {}> = {
    datas?: Array<D>;
    modelValue?: boolean;
    step?: number | string;
    limitScrollNum?: number | string;
    hover?: boolean;
    direction?: string;
    isWatch?: boolean;
    count?: number | string;
    duration?: number | string;
    cubicBezier?: {
      x1?: number;
      y1?: number;
      x2?: number;
      y2?: number;
    }
  }

  type jsOptions<D = {}> = {
    modelValue?: boolean;
    datas?: Array<D>;
    step?: number | string;
    limitScrollNum?: number | string;
    hover?: boolean;
    direction?: string;
    singleHeight?: number | string;
    singleWidth?: number | string;
    singleWaitTime?: number | string;
    isRemUnit?: boolean;
    isWatch?: boolean;
    delay?: number | string;
    ease?: string;
  }

  export const jsSeamlessScroll: import("vue").DefineComponent<jsOptions>;

  export const cssSeamlessScroll: import("vue").DefineComponent<cssOptions>;

  const install: (app: import("vue").App, options?: {
    name?: string
  }) => any;

  export default install;
}
